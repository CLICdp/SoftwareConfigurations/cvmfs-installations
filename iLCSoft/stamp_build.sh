#!/bin/bash

export stamp=`date -u`

if [[ ${COMPILER_TYPE} == "gcc" && ${BUILD_PATH} == "nightly" ]]; then
    echo "echo \"----------------------------------------------\"" >> /cvmfs/clicdp.cern.ch/iLCSoft/builds/nightly/x86_64-slc6-gcc62-opt/init_ilcsoft.sh
    echo "echo \"Build created on ${stamp}\"" >> /cvmfs/clicdp.cern.ch/iLCSoft/builds/nightly/x86_64-slc6-gcc62-opt/init_ilcsoft.sh
    echo "echo \"----------------------------------------------\"" >> /cvmfs/clicdp.cern.ch/iLCSoft/builds/nightly/x86_64-slc6-gcc62-opt/init_ilcsoft.sh
fi

if [[ ${COMPILER_TYPE} == "llvm" && ${BUILD_PATH} == "nightly" ]]; then
    echo "echo \"----------------------------------------------\"" >> /cvmfs/clicdp.cern.ch/iLCSoft/builds/nightly/x86_64-slc6-llvm39-opt/init_ilcsoft.sh
    echo "echo \"Build created on ${stamp}\"" >> /cvmfs/clicdp.cern.ch/iLCSoft/builds/nightly/x86_64-slc6-llvm39-opt/init_ilcsoft.sh
    echo "echo \"----------------------------------------------\"" >> /cvmfs/clicdp.cern.ch/iLCSoft/builds/nightly/x86_64-slc6-llvm39-opt/init_ilcsoft.sh
fi
