#!/bin/bash

#Delete build results from previous stage
rm -f /eos/project/l/lcd-web/www/iLCSoft_builds/ilcsoft_gcc.tar
rm -f /eos/project/l/lcd-web/www/iLCSoft_builds/ilcsoft_llvm.tar

#Stop and remove the container if present
docker kill ilcsoft_builder
docker rm ilcsoft_builder

#Star the container in detached mode
docker run -it --name ilcsoft_builder -e COMPILER_TYPE='gcc' -e COMPILER_VERSION='gcc62' -e BUILD_PATH='nightly' -d -v /cvmfs/clicdp.cern.ch/compilers/:/cvmfs/clicdp.cern.ch/compilers/ -v /cvmfs/clicdp.cern.ch/software/:/cvmfs/clicdp.cern.ch/software/ clicdp/slc6-build /bin/bash

#Download the build instruction from gitlab repo
docker exec ilcsoft_builder /bin/bash -c "wget https://gitlab.cern.ch/CLICdp/SoftwareConfigurations/cvmfs-installations/raw/master/iLCSoft/make_build.sh"

#Execute the building
docker exec ilcsoft_builder /bin/bash -c "chmod a+x make_build.sh ; ./make_build.sh"

#Copy the resulting tar ball form the container
docker cp ilcsoft_builder:/ilcsoft_gcc.tar /eos/project/l/lcd-web/www/iLCSoft_builds/ilcsoft_gcc.tar
docker cp ilcsoft_builder:/ilcsoft_gcc.log /eos/project/l/lcd-web/www/iLCSoft_builds/ilcsoft_gcc.log
#Set Build badge
if [ -e /eos/project/l/lcd-web/www/iLCSoft_builds/ilcsoft_gcc.tar ]
then
    cp /eos/project/l/lcd-web/www/iLCSoft_builds/badges/passing.svg /eos/project/l/lcd-web/www/iLCSoft_builds/ilcsoft_gcc_status.svg
else
    cp /eos/project/l/lcd-web/www/iLCSoft_builds/badges/failing.svg /eos/project/l/lcd-web/www/iLCSoft_builds/ilcsoft_gcc_status.svg
fi


#Stop and remove the container
docker kill ilcsoft_builder
docker rm ilcsoft_builder


#Star the container in detached mode
docker run -it --name ilcsoft_builder -e COMPILER_TYPE='llvm' -e COMPILER_VERSION='llvm39' -e BUILD_PATH='nightly' -d -v /cvmfs/clicdp.cern.ch/compilers/:/cvmfs/clicdp.cern.ch/compilers/ -v /cvmfs/clicdp.cern.ch/software/:/cvmfs/clicdp.cern.ch/software/ clicdp/slc6-build /bin/bash

#Download the build instruction from gitlab repo
docker exec ilcsoft_builder /bin/bash -c "wget https://gitlab.cern.ch/CLICdp/SoftwareConfigurations/cvmfs-installations/raw/master/iLCSoft/make_build.sh"

#Execute the building
docker exec ilcsoft_builder /bin/bash -c "chmod a+x make_build.sh ; ./make_build.sh"

#Copy the resulting tar ball form the container
docker cp ilcsoft_builder:/ilcsoft_llvm.tar /eos/project/l/lcd-web/www/iLCSoft_builds/ilcsoft_llvm.tar
docker cp ilcsoft_builder:/ilcsoft_llvm.log /eos/project/l/lcd-web/www/iLCSoft_builds/ilcsoft_llvm.log
#Set Build badge
if [ -e /eos/project/l/lcd-web/www/iLCSoft_builds/ilcsoft_llvm.tar ]
then
    cp /eos/project/l/lcd-web/www/iLCSoft_builds/badges/passing.svg /eos/project/l/lcd-web/www/iLCSoft_builds/ilcsoft_llvm_status.svg
else
    cp /eos/project/l/lcd-web/www/iLCSoft_builds/badges/failing.svg /eos/project/l/lcd-web/www/iLCSoft_builds/ilcsoft_llvm_status.svg
fi

#Stop and remove the container
docker kill ilcsoft_builder
docker rm ilcsoft_builder
