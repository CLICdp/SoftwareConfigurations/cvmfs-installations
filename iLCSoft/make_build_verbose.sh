#!/bin/bash

source /cvmfs/clicdp.cern.ch/software/git/2.10.2/x86_64-slc6-gcc62-opt/setup.sh
git clone https://github.com/iLCSoft/iLCInstall.git ilcinstall && \
cd ilcinstall && \
source builds/init_x86_64.sh && \
./ilcsoft-install builds/release-ilcsoft.cfg -p > /dev/null && \
./ilcsoft-install builds/release-ilcsoft.cfg -i  && \
cd / && \
wget https://gitlab.cern.ch/CLICdp/SoftwareConfigurations/cvmfs-installations/raw/master/iLCSoft/stamp_build.sh && \
chmod a+x stamp_build.sh && \
./stamp_build.sh && \
tar cvf ilcsoft_${COMPILER_TYPE}.tar --exclude .git --exclude .svn --exclude build /cvmfs/clicdp.cern.ch/iLCSoft/builds/
