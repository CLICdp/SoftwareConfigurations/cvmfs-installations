#!/bin/bash


if [ "$( cat /etc/*-release | grep Scientific )" ]; then
    OS=slc6
elif [ "$( cat /etc/*-release | grep CentOS )" ]; then
    OS=centos7
else
    echo "UNKNOWN OS"
    exit 1
fi

#Install required packages
yum install bzip2 bzip2-devel curl curses curses-devel db4 db4-devel dbm-devel expat expat-devel gdbm gdbm gdbm-devel libdb libdb-* libffi libffi-devel libjpeg libjpeg-devel libpng libpng-devel libxml2-devel libxslt-devel libxslt-devel lzma mysql mysql-devel MySQL-python MySQL-python-devel openldap openldap-devel openssl openssl-devel patch readline readline readline-devel sqlite sqlite-devel tcl tcl-devel tk tk-devel xz-devel zlib zlib-devel -y

echo "Setup your python e.g."
echo "export PYTHONDIR=/cvmfs/clicdp.cern.ch/software/Python/2.7.12/x86_64-slc6-gcc48-dbg/"
echo "export PATH=$PYTHONDIR/bin:$PATH"
echo "export LD_LIBRARY_PATH=$PYTHONDIR/lib:$LD_LIBRARY_PATH"
echo "export PYTHONPATH=/cvmfs/clicdp.cern.ch/software/pytools/2.1/x86_64-slc6-gcc48-dbg/lib/python2.7/site-packages"
echo ""
echo "Install all packages via pip"
echo "pip install --install-option=\"--prefix=/cvmfs/clicdp.cern.ch/software/pytools/2.1/x86_64-slc6-gcc48-dbg\" -r requirements.txt

