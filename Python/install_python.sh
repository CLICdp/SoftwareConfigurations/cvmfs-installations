#!/bin/bash


if [ "$( cat /etc/*-release | grep Scientific )" ]; then
    OS=slc6
elif [ "$( cat /etc/*-release | grep CentOS )" ]; then
    OS=centos7
else
    echo "UNKNOWN OS"
    exit 1
fi

if [ -z ${PYTHON_VERSION} ]; then
    echo "Please set PYTHON_VERSION variable"
    exit 1
fi

if [ -z ${BUILD_TYPE} ]; then
    echo "Please set BUILD_TYPE variable"
    exit 1
    
fi

if [ -z ${INSTALL_PREFIX} ]; then
    echo "Please set INSTALL_PREFIX variable"
    exit 1
fi


#Install required packages
yum install bzip2 bzip2-devel curl curses curses-devel db4 db4-devel dbm-devel expat expat-devel gdbm gdbm gdbm-devel libdb libdb-* libffi libffi-devel libjpeg libjpeg-devel libpng libpng-devel libxml2-devel libxslt-devel libxslt-devel lzma mysql mysql-devel MySQL-python MySQL-python-devel openldap openldap-devel openssl openssl-devel patch readline readline readline-devel sqlite sqlite-devel tcl tcl-devel tk tk-devel xz-devel zlib zlib-devel -y

# Get the source
wget https://www.python.org/ftp/python/$PYTHON_VERSION/Python-$PYTHON_VERSION.tgz

#Extract the source
tar xvf Python-$PYTHON_VERSION.tgz

cd Python-$PYTHON_VERSION


if [ ${BUILD_TYPE}="opt" ]; then
	export CFLAGS="-03"
    ./configure --enable-shared --enable-ipv6 --preifx=$INSTALL_PREFIX
    make -j4
fi

if [ ${BUILD_TYPE}="dbg" ]; then
	export CFLAGS="-g"
    ./configure --enable-shared --enable-ipv6 --with-pydebug --preifx=$INSTALL_PREFIX
    make -j4
fi
   
echo "If compilation sucefull call make install"
 








