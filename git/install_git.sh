#!/bin/bash


if [ "$( cat /etc/*-release | grep Scientific )" ]; then
    OS=slc6
elif [ "$( cat /etc/*-release | grep CentOS )" ]; then
    OS=centos7
else
    echo "UNKNOWN OS"
    exit 1
fi

#Install required packages
yum install curl-devel expat-devel gettext-devel openssl-devel perl-devel zlib-devel docbook2X asciidoc xmlto -y

ln -s /usr/bin/db2x_docbook2texi /usr/bin/docbook2x-texi

echo "Setup your git e.g."
echo "export CFLAGS=\"-O3\""
echo "./configure --prefix=_path_"
echo "EDIT Makefile replace all hard links to soft links (ln)->(ln -s) otherwise cvmfs does not support it"
echo "make -j4; make man -j4; make install; make install-man

